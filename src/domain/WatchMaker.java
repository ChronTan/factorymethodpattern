package domain;

public interface WatchMaker {
    Watch createWatch();
}
