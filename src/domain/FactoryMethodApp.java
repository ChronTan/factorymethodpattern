package domain;

public class FactoryMethodApp {
    public static void main(String[] args) {
        WatchMaker watchMaker=new DigitalWatchMaker();
        Watch watch=watchMaker.createWatch();
        watch.showTime();
    }
}
