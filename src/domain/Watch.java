package domain;

public interface Watch {
    void showTime();
}
